class AddUserIdToCats < ActiveRecord::Migration
  def change
    add_column :cats, :user_id, :integer, :references => :users
    add_index :cats, :user_id
  end
end
