class CreateCats < ActiveRecord::Migration
  def change
    create_table :cats do |t|
      t.integer :age
      t.date :birth_date
      t.string :color
      t.string :name
      t.string :sex, :length => 1

      t.timestamps
    end
    add_index :cats, :name
  end
end
