class CreateSessionTokens < ActiveRecord::Migration
  def change
    create_table :session_tokens do |t|
      t.string :session_token, :null => false
      t.integer :user_id, :null => false, :references => :users
      t.string :device
      t.string :login_location

      t.timestamps
    end
    add_index :session_tokens, :session_token, :unique => true
  end
end
