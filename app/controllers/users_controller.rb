class UsersController < ApplicationController
  before_action :require_no_login, only: [:new, :create]
  before_action :require_login, only: [:show, :edit, :destroy, :update]

  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login!(@user)
      redirect_to user_url(@user)
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
    @cats = Cat.where(:user_id => @user.id)
    @sessions = SessionToken.where(:user_id => @user.id)
    render :show
  end

  def index
    @users = User.all
    render :index
  end

  def edit
    @user = User.find(params[:id])
    render :edit
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(user_params)
    if @user.save
      @user.reset_session_token!
      redirect_to user_url(@user)
    else
      flash.now[:errors] = @user.errors.full_messages
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    logout!
    @user.destroy!
  end

  private
  def user_params
    params.require(:user).permit(:user_name, :password)
  end
end
