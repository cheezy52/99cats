class CatsController < ApplicationController
  before_action :require_login, only: [:new, :edit, :destroy, :update, :create]
  before_action :verify_cat_ownership, only: [:edit, :update, :destroy]

  def index
    @cats = Cat.all
    render :index
  end

  def show
    @cat = Cat.find(params[:id])
    @user = User.find(@cat.user_id)
    @requests = CatRentalRequest.where(:cat_id => @cat.id)
      .where("status = 'PENDING' OR status = 'APPROVED'")
      .order(:start_date)
    render :show
  end

  def edit
    @cat = Cat.find(params[:id])
    @colors = Cat::VALID_COLORS
    render :edit
  end

  def new
    @cat = Cat.new
    @colors = Cat::VALID_COLORS
    render :new
  end

  def create
    @cat = Cat.new(cat_params)
    @cat.user_id = current_user.id
    @colors = Cat::VALID_COLORS
    if @cat.save
      redirect_to cat_url(@cat)
    else
      flash.now[:errors] = @cat.errors.full_messages
      render :new
    end
  end

  def destroy
    @cat = Cat.find(params[:id])
    if @cat
      @cat.destroy
    else
      flash[:errors] = "No cat with that id"
    end
    redirect_to cats_url
  end

  def update
    @cat = Cat.find(params[:id])
    @cat.update_attributes(cat_params)
    @colors = Cat::VALID_COLORS
    if @cat.save
      redirect_to cat_url(@cat)
    else
      flash.now[:errors] = @cat.errors.full_messages
      render :edit
    end
  end

  private
  def cat_params
    params.require(:cat).permit(:age, :name, :color, :sex, :birth_date)
  end

end
