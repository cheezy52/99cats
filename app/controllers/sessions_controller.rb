class SessionsController < ApplicationController
  before_action :require_no_login, only: [:new, :create]
  before_action :require_login, only: [:destroy]

  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.find_by_credentials(session_params)
    login_location = get_user_location(@user)
    device = get_user_device(@user)
    login!(@user, login_location, device)
    if @user
      redirect_to user_url(@user)
    else
      render :new
    end
  end

  def destroy
    token = SessionToken.find(params[:id])
    logout!(token)
    if current_user
      redirect_to user_url(current_user)
    else
      redirect_to new_session_url
    end
  end

end
