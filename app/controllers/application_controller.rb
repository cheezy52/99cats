class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  include CatsHelper

  def get_user_device(user)
    browser = request.env['HTTP_USER_AGENT'].downcase
    if browser.match(/mozilla|chrome|safari/)
      return 'Desktop'
    elsif browser.match(/android|iphone/)
      return 'Mobile'
    elsif browser.match(/ipod/)
      return 'Tablet'
    end
    'Unknown device'
  end

  def get_user_location(user)
    #ip = request.env['REMOTE_ADDR']
    ip = request.remote_ip
    location = Geocoder.address(ip)
    location
  end
end
