class CatRentalRequestsController < ApplicationController
  before_action :verify_cat_ownership, only: [:edit, :update, :destroy]
  before_action :require_login

  def new
    @cats = Cat.all
    @request = CatRentalRequest.new
    render :new
  end

  def create
    @cats = Cat.all
    @request = CatRentalRequest.new(request_params)
    if @request.save
      redirect_to(cat_url(Cat.find(@request.cat_id)))
    else
      flash.now[:errors] = @request.errors.full_messages
      render :new
    end
  end

  def index
    @requests = CatRentalRequest.all
    @cats = Cat.all
    render :index
  end

  def show
    @request = CatRentalRequest.find(params[:id])
    @cat = Cat.find(@request.cat_id)
    render :show
  end

  def update
    @cats = Cat.all
    @request = CatRentalRequest.find(params[:id])
    @request.update_attributes(request_params)
    if @request.save
      redirect_to(cat_rental_request_url(@request))
    else
      flash.now[:errors] = @request.errors.full_messages
      render :edit
    end
  end

  def edit
    @cats = Cat.all
    @request = CatRentalRequest.find(params[:id])
    render :edit
  end

  def destroy
    @request = CatRentalRequest.find(params[:id])
    @cat = Cat.find(@request.cat_id)
    if @request
      @request.destroy
    else
      flash[:errors] = "No request with that id"
    end
    redirect_to(cat_url(@cat))
  end

  def approve
    @request = CatRentalRequest.find(params[:cat_rental_request_id])
    @cat = Cat.find(@request.cat_id)
    @request.approve! if @cat.user_id == current_user.id
    flash[:errors] = @request.errors.full_messages
    redirect_to(cat_url(@cat))
  end

  def deny
    @request = CatRentalRequest.find(params[:cat_rental_request_id])
    @cat = Cat.find(@request.cat_id)
    @request.deny! if @cat.user_id == current_user.id
    flash[:errors] = @request.errors.full_messages
    redirect_to(cat_url(@cat))
  end

  private
  def request_params
    params.require(:request).permit(:cat_id, :start_date, :end_date, :status)
  end
end
