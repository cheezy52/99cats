module CatsHelper
  def verify_cat_ownership
    redirect_to cats_url unless cat_owner?
  end

  def cat_owner?
    Cat.find(params[:id]).user_id == current_user.id
  end


end
