module SessionsHelper

  def current_user
    token = SessionToken.find_by_session_token(session[:session_token])
    if token
      @current_user ||= User.find(token.user_id)
    else
      nil
    end
  end

  def login!(user, login_location, device)
    @current_user = user
    token = SessionToken.create_session_token(user, login_location, device)
    token.save!
    session[:session_token] = token.session_token
  end

  def logout!(token)
    if session[:session_token] == token
      session[:session_token] = nil
      @current_user = nil
    end
    token.destroy!
  end

  def session_params
    params.require(:user).permit(:user_name, :password)
  end

  def logged_in?
    !!current_user
  end

  def require_login
    redirect_to cats_url unless logged_in?
  end

  def require_no_login
    redirect_to cats_url if logged_in?
  end
end
