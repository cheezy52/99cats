class Cat < ActiveRecord::Base
  VALID_COLORS = %w(black gray calico cream white orange vibrantpurple)

  validates :age, :birth_date, :color, :name, :sex, :user_id, :presence => true
  validates :sex, :inclusion => { in: ['M', 'F']}
  validates :color, :inclusion => { in: VALID_COLORS }
  validates :age, :numericality => true

  has_many :cat_rental_requests, :dependent => :destroy
  has_one :owner
end
