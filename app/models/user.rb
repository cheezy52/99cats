class User < ActiveRecord::Base
  before_validation :reset_session_token!
  validates :user_name, :session_token, :uniqueness => true, :presence => true

  has_many :cats, :dependent => :destroy
  has_many :session_tokens, :dependent => :destroy


  def reset_session_token!
    self.session_token = SecureRandom::urlsafe_base64(16)
  end

  def password=(plaintext)
    self.password_digest = BCrypt::Password.create(plaintext)
  end

  def is_password?(plaintext)
    BCrypt::Password.new(self.password_digest).is_password?(plaintext)
  end

  def self.find_by_credentials(params)
    user = User.find_by_user_name(params[:user_name])
    if user
      return user if user.is_password?(params[:password])
    end
    nil
  end
end
