class CatRentalRequest < ActiveRecord::Base
  VALID_STATUS = ['PENDING', 'APPROVED', 'DENIED']

  before_validation(on: :create) {self.status ||= "PENDING"}
  validates :status, :inclusion => { in: VALID_STATUS }
  validates :cat_id, :start_date, :end_date, :status, presence: true
  validate :requests_do_not_overlap
  validate :start_date_before_end_date

  belongs_to :cat

  def start_date_before_end_date
    unless start_date <= end_date
      errors[:cat_rental_request] << "must not have end date before start date"
    end
  end

  def requests_do_not_overlap
    if !overlapping_approved_requests.empty? && self.status == "APPROVED"
      errors[:cat_rental_request] << "cannot have overlapping approved requests"
    end
  end

  def overlapping_requests
    CatRentalRequest.where(:cat_id => self.cat_id)
      .where("id <> ?", self.id)
      .where('end_date > ? AND start_date < ?', self.start_date, self.end_date)
  end

  def overlapping_approved_requests
    overlapping_requests.where(:status => 'APPROVED')
  end

  def overlapping_pending_requests
    @requests = overlapping_requests.where(:status => 'PENDING')
  end

  def approve!
    self.transaction do
      self.status = "APPROVED"
      self.save
      overlapping_pending_requests.each do |request|
        request.deny!
      end
    end
  end

  def deny!
    self.status = "DENIED"
    self.save
  end
end
