class SessionToken < ActiveRecord::Base
  validates :user_id, :presence => true
  validates :session_token, :presence => true, :uniqueness => true

  belongs_to :user

  def self.create_session_token(user, login_location, device)
    session = self.new
    session.session_token = SecureRandom::urlsafe_base64(16)
    session.user_id = user.id
    session.login_location = login_location
    session.device = device
    session
  end
end
