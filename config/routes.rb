Cats99::Application.routes.draw do
  resources :users
  resources :sessions, :only => [:new, :create, :destroy]
  resources :cats
  resources :cat_rental_requests do
    patch :approve
    patch :deny
  end
  root :to => 'cats#index'
end
